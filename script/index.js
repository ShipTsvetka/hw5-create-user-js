'use strict'

let firstName = prompt('Enter your first name');
let lastName = prompt('Enter your last name');

let createNewUser = (firstName, lastName) => {

    const newUser = {
        firstName,
        lastName,
        getLogin () {
            return this.firstName.slice(0, 1).toLocaleLowerCase() + this.lastName.toLocaleLowerCase();
        },
    }

    return newUser;

};
 const user = createNewUser(firstName, lastName);
 console.log(user.getLogin());

///////////////////////////////////////////////////////////////////////////////////////////////////
/*

const createNewUser = () => {
    const firstName = prompt('Enter your first name');
    const lastName = prompt('Enter your last name');

    return newUser = {
        firstName,
        lastName,
        getLogin() {
            return login = (this.firstName.slice(0, 1) + this.lastName).toLowerCase();
        },
    };

};

createNewUser();
const user = Object.assign({}, newUser);
console.log(user.getLogin());

*/
//////////////////////////////////////////////////////////////////////////////////////////////////
/*

1.Опишіть своїми словами, що таке метод об'єкту:

   Метод - це функція, вбудована в об'єкт, яка працює з даними обє'кта та значеннями, які отримує в вигляді аргументів.

2.Який тип даних може мати значення властивості об'єкта?

   Будь-який.

3.Об'єкт це посилальний тип даних. Що означає це поняття?

    При копіюванні об'єкта не створюється нова область видимості, а лише посилання на нього. При змінах всі дані підтягуються в копію.
    В змінну зберігається лише посилання на об'єкт, а дані об'єкту збережені окремо.

*/